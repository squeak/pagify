window.jQuery = $;
require("jquery-colorbox");
var _ = require("underscore");
var $$ = require("squeak");
require("squeak/plugin/random.position");
var pagify = require("./_");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  THUMBNAIL
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function thumbnail (indexedPage, options) {
  // ---------------------------------------------------- DEFAULT OPTIONS END
  var name = indexedPage.title;
  if (!name) return console.log("floatingThumbnail problem, no name");
  var defaultOptions = {
    referenceContainer: options.$container,
    size: 150,
    color: "black",
    colorHover: "white",
    backgroundColor: function(){ return $$.random.color(); },
    backgroundColorHover: function(){ return $$.random.color(); },
  };
  options = $$.defaults(defaultOptions, options);


  var link = options.$container.a({
    href: indexedPage.url,
    class: "pagify-floating_index",
    id: "pagify-floating_index-"+ name,
    html: "<br/>"+ (indexedPage.title ? indexedPage.title : name) + "<br/><br/>",
  }).css({
    width: options.size,
    //height: 50,
    //paddingTop: 20,
    //paddingBottom: 20,
    //padding: 5,
    textAlign: "center",
    position: "absolute",
    display: "block",
    // left: $$.random.number(0, all.width() - options.size),
    // top: $$.random.number(0, all.height() - options.size*1.5), // -size*1.5 is estimation of what it can be with image so that that there is no overflow
    color: $$.result(options.color),
    backgroundColor: $$.result(options.backgroundColor),
  }).hover(function(){
    $(this).css({
      color: $$.result(options.colorHover),
      backgroundColor: $$.result(options.backgroundColorHover),
    });
  }, function(){
    $$.random.position(options.referenceContainer, $(this), $(".pagify-floating_index"));
    // setRandomPos($(".pagify-floating_index"),40,40);
    // $(this).css({
    //   color: $$.result(options.color),
    //   backgroundColor: $$.result(options.backgroundColor),
    //   left: $$.random.number(0, all.width() - $(this).width()),// -20), // -20 probably necessary for padding
    //   top: $$.random.number(0, all.height() - $(this).height()),// -20), // -20 probably necessary for padding
    // });
    $(this).css({
      color: $$.result(options.color),
      backgroundColor: $$.result(options.backgroundColor),
    });
  });
  // optional click event
  if (_.isFunction(indexedPage.click)) link.click(indexedPage.click);
  // image
  var $linkImg = link.img({
    src: indexedPage.thumbnailPath || indexedPage.data +"thumbnail.png",
    width: "100%",
  });

  // lock or folder
  if (indexedPage && indexedPage.auth) link.div({
    class: "entry_float-custom_auth icon-lock",
  });
  if (indexedPage && indexedPage.isFolder) link.div({
    class: "entry_float-custom_folder icon-folder",
  });

  // set random position
  var $floatingThumbnails = $(".pagify-floating_index");
  $linkImg.on("load", function () {
    $$.random.position(options.referenceContainer, link, $floatingThumbnails);
  });

  // onload(function(){ $$.random.position(options.referenceContainer, link, $(".pagify-floating_index")); });

  // execute some custom function on created thumbnail
  if (indexedPage.openInColorbox) link.colorbox(indexedPage.openInColorbox);

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: create clickable thumbnail to create an index of given pages (the thumbnails are displayed randomly in the page)
  ARGUMENTS: ({
    !pages: <{ url: <string>, title: <string>, }[]>,
    !$container <yquerj>,
    ?size: <number @default=150>,
    ?color: <string @default="black">,
    ?colorHover: <string @default="white">,
    ?backgroundColor: <string|function(ø):string>,
    ?backgroundColorHover: <string|function(ø):string>,
    ?openInColorbox: <>
    ... MAYBE SOME OTHER OPTIONS, TO CHECK
  })
  RETURN: void
*/
pagify.floating = function (options) {
  _.each(options.pages, function (page) {
    if (page.inIndex !== false) thumbnail(page, options); // display thumbnail if page is indexable
    // var data = "data/pages/"+ d.url;
  });
};

// //
// //                              PAGE
//
// /**
//   DESCRIPTION: make a page with a random background, title, and pages
//   ARGUMENTS: ({
//     pages: <object> « list of pages »,
//     title: <string>,
//     backgroundsPaths: <string> « path to find backgrounds »,
//   })
//   RETURN: ø
// */
// pagify.floating.page: function (options) {
//
//   // name and title
//   options.$container.div({
//     class: "pagify-index-name",
//     html: page.name,
//   });
//   options.$container.div({
//     class: "pagify-index-title",
//     html: options.title,
//   });
//
//   // pages
//   pagify.floating(options.pages);
//
// },

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = pagify.floating;

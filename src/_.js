var _ = require("underscore");
var $$ = require("squeak");
require("squeak/extension/dom");
var $ = require("yquerj");
var debounce = require("../lib/throttle-debounce.js").debounce;

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  INITIALIZE
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: create a new pagify instance (without creating any page)
  ARGUMENTS: (
    appendTo <jqueryObject|jqueryString>,
    options <{
      color: <string> « color of the interface »,
      indicatorOptions: <{}> « see pagify.indicator.create for detail about those options »,
    }>
  )
  RETURN: pagy
*/
function pagifyInitialize (appendTo, options) {

  // MAKE SURE OPTIONS IS AN OBJECT
  if (!_.isObject(options)) options = {};

  // default options
  var defaultIndicatorOptions = {
    size: 20,
    color: "cyan",
  };

  // o            >            +            #            °            ·            ^            :            |
  //                                           CREATE PAGY OBJECT FOR THIS INSTANCE

  var pagy = {

    // append container of pagify in selected div
    container: $(appendTo).div({
      class: "pagify-container",
      id: pagify.instances.length,
    }),

    // set this pagy ID for further selection and identification of pagy
    id: pagify.instances.length,

    // store gallery and index for usage in resize
    gallery: [],
    galleryOptions: [],
    index: [],
    indexOptions: [],

    panIdAsked: 0,
    panid: 0,
    panidNeg: 0,

    // pages creation functions
    createPage: {},

    // set color from options
    color: options.color || "white",

    indicators: {

      // $container: <jquery> « created by pagify.indicator.create »,

      options: $$.defaults(defaultIndicatorOptions, options.indicatorOptions), // « defaults added by pagify.indicator.create »

    },

    //
    //                              FINALIZE

    finalize: function () {

      pagy.maxPanid = pagy.panid - 1;
      pagy.minPanid = pagy.panidNeg;

      pagy.pan.css({ left: pagy.container.width() * -pagy.panIdAsked });
      if (!(pagy.maxPanid == 0 && pagy.minPanid == 0) && !pagy.navigationArrows) pagify.navigation.arrows(pagy);

      pagy.setContainerDimension();

    },

    //
    //                              SET CONTAINER DIMENSION

    setContainerDimension: function () {

      // GENERAL STYLES
      pagy.container.css({
        width: pagy.container.parent().width(),
        height: pagy.container.parent().height(),
      });

      pagy.pan.children().css({
        width: pagy.container.width(),
        height: pagy.container.height(),
        left: function () { return pagy.container.width() * +$(this).attr("panid") },
      });

    },

    //
    //                              RESIZE

    resize: function () {

      // SET CONTAINER DIMENSION
      pagy.setContainerDimension();

      // REMOVE PAGES AND INDICATORS
      pagy.pan.empty();
      pagy.indicators.$container.empty();
      pagy.panid = 0;
      pagy.panidNeg = 0;
      delete pagy.maxPanid;
      delete pagy.minPanid;

      // RECREATE PAGES
      pagy.maker.call(pagy);
      pagy.maxPanid = pagy.panid - 1;
      pagy.minPanid = pagy.panidNeg;

      // check that we are not seeing a page that doesn't exist anymore because gallery number of page changed
      if (pagy.panIdAsked > pagy.maxPanid) pagify.navigation.pan(pagy, pagy.maxPanid)
      else pagify.navigation.pan(pagy, pagy.panIdAsked);

    },

    //                              ¬
    //

  };

  //                                           ...
  // o            >            +            #            °            ·            ^            :            |

  // add models to pagy object
  _.each(pagify.models, function(modelFunction, modelName){
    pagy.createPage[modelName] = _.bind(modelFunction, pagy);
  });

  // store target so that all pagified divs can be inventoried
  pagify.instances.push(pagy);

  // session storage panid storing name // TODO restart using session storage??
  // ssiName.push(page.url + "__pagifyIn-"+ pagy.id +"__panidAsked");

  // set panIdAsked if not existing
  // ssi(ssiName[pagy.id], 0, "if");
  // set panId limits

  // append pan
  pagy.pan = pagy.container.div({ class: "pagify-pan", });

  // initialize shortcuts and navigation elements
  pagify.navigation.shortcuts(appendTo, pagy);

  // on resize events
  pagifyResize(pagy);

  return pagy;

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  ON RESIZE
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function pagifyResize (pagy) {

  // append loading wheel
  $$.dom.onWindowResize(debounce(500, true, function () {
    pagy.$galleryLoadingWheel = pagy.container.div({
      class: "pagify-gallery-loading_wheel animate-spin",
    }).div({
      class: "icon-spinner102 animate-color",
    });
  }));

  // reappend gallery
  $$.dom.onWindowResize(debounce(500, function () {
    // resize
    pagy.resize();
    // remove loading wheels
    pagy.$galleryLoadingWheel.remove();
  }));

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

var pagify = $$.scopeFunction({

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  INSTANCES
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  instances: [],

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  MAIN
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION:
    ARGUMENTS: (
      appendTo <jqueryObject|jqueryString>,
      pageType: <key of pagify.models>,
      data: <> « depend on the type of page you want to display »,
      options <{
        color: <string> « color of the interface »,
        indicatorOptions: <{}> « see pagify.indicator.create for detail about those options »,
      }>
    )
    RETURN:
  */
  main: function (appendTo, pageType, data, options) {

    //
    //                              INITIALIZE PAGY

    var pagy = pagifyInitialize(appendTo, options);

    //
    //                              DISPLAY ASKED TYPE OF PAGE

    pagy.maker = function () {
      var pagy = this;
      if (pageType == "empty") pagy.createPage[pageType](options)
      else if (pageType == "gallery") pagy.createPage[pageType](data, options)
      else pagy.createPage[pageType](data, options.indicatorOptions);
    };

    pagy.maker.call(pagy);

    //
    //                              FINALIZE PAGY

    pagy.finalize();
    return pagy;

    //                              ¬
    //

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  CUSTOM
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION:
    ARGUMENTS: ({
      $container: <yquerjObject>,
      maker: <function> « this function is called to make the pages (it's recalled on resize events as well) pagy is be passed as "this" of this function when called »,
      options: « any options to pass to pagifyInitialize »,
    })
    RETURN:
  */
  custom: function (config) {

    // INITIALIZE
    var pagy = pagifyInitialize(config.$container, config.options);

    // MAKE PAGES
    pagy.maker = config.maker;
    pagy.maker.call(pagy);

    // FINALIZE
    pagy.finalize();

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

});

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = pagify;

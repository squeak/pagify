var _ = require("underscore");
var $$ = require("squeak");
var pagify = require("./_");
var keyboardify = require("keyboardify");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

pagify.navigation = {

  //
  //                              NEXT, PREVIOUS, GO TO PAGE

  goToPage: function (pagy, idAsked) {
    pagify.navigation.pan(pagy, idAsked);
    // ssi(ssiName[pagy.id], idAsked)
    pagy.panIdAsked = idAsked;
  },
  nextPage: function (pagy) {
    var idAsked = +pagy.panIdAsked + 1;
    if (idAsked > pagy.maxPanid) idAsked = pagy.minPanid;
    pagify.navigation.goToPage(pagy, idAsked);
  },
  previousPage: function (pagy) {
    var idAsked = +pagy.panIdAsked - 1;
    if (idAsked < pagy.minPanid) idAsked = pagy.maxPanid;
    pagify.navigation.goToPage(pagy, idAsked);
  },

  //
  //                              PAN

  pan: function (pagy, idAsked) {
    pagify.indicator.change(pagy, idAsked);
    pagy.container.find(".pagify-pan").css({ left: pagy.container.width() * -idAsked });
  },

  //
  //                              ARROWS

  arrows: function (pagy) {

    pagy.navigationArrows = {
      container: pagy.container.div({"class": "pagify-navigation-arrows"}).css("color", pagy.color || "white"),
    };

    ["left", "right"].forEach(function(side,i){
      pagy.navigationArrows[side] = pagy.navigationArrows.container.div({
        class: "pagify-navigation-arrow pagify-navigation-arrow-"+ side +" icon-arrow-"+ side +"3",
      });
    });

    pagy.navigationArrows.left.click(function(){ pagify.navigation.previousPage(pagy) }).addClass("INLEFT");
    pagy.navigationArrows.right.click(function(){ pagify.navigation.nextPage(pagy) });

  },

  //
  //                              SHORTCUTS

  shortcuts: function (appendTo, pagy) {

    // clear previous key navigation bindings and set new ones
    keyboardify.off("right");
    keyboardify.off("left");
    keyboardify.on("right", function() { if ($("#colorbox").css("display") != "block") pagify.navigation.nextPage(pagy); }); // check if colorbox is opened
    keyboardify.on("left", function() { if ($("#colorbox").css("display") != "block") pagify.navigation.previousPage(pagy); }); // check if colorbox is opened
    // set keyboard shortcuts to last selected:clicked div
    pagy.container.click(function(){ pagify.navigation.keyboard(appendTo, pagy) });

    // var hammerPagesOfMenus = new Hammer(to[0]);
    // //hammerPagesOfMenus.on('panright', function(ev) { PANing(to, ev.deltaX) });
    // //hammerPagesOfMenus.on('panleft', function(ev) { PANing (to, ev.deltaX) });
    // hammerPagesOfMenus.on('panright', function(ev) { if (ev.isFinal) pagify.navigation.previousPage(to); });
    // hammerPagesOfMenus.on('panleft', function(ev) { if (ev.isFinal) pagify.navigation.nextPage(to); });
    //
    // hammerPagesOfMenus.on('swiperight', function(ev) { pagify.navigation.previousPage(to) });
    // hammerPagesOfMenus.on('swipeleft', function(ev) { pagify.navigation.nextPage(to) });

  },

  keyboard: function (appendTo, pagy) {
    keyboardify.off("right");
    keyboardify.off("left");
    keyboardify.on("right", function() { if ($("#colorbox").css("display") != "block") pagify.navigation.nextPage(pagy); }); // check if colorbox is opened
    keyboardify.on("left", function() { if ($("#colorbox").css("display") != "block") pagify.navigation.previousPage(pagy); }); // check if colorbox is opened
  },


  //                              ¬
  //

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = pagify.navigation;

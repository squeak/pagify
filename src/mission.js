var _ = require("underscore");
var $$ = require("squeak");
require("squeak/extension/ajax");
require("squeak/extension/url");
var pagify = require("./_");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  GETTING MATERIALS
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

var get = {

  //
  //                              TAGS

  tags: function (options, next) {

    if (!options.tags) $$.ajax.get({
      url: options.path +"tags.json",
      success: next,
      error: function (response) {
        $$.alert.detailedError(
          "pagify.mission/get.tags",
          "Error trying to get tags.",
          response
        );
        next({});
      },
    })

    else next(options.tags);

  },

  //
  //                              DESCRIPTION

  description: function (folderPath, tags, descriptionKey, next) {
    // get description from tags
    if (descriptionKey) next(tags[descriptionKey])
    // fetch description
    else $$.ajax.get({
      url: $$.url.urlify(folderPath +"description"),
      success: next,
      error: function (response) {
        $$.alert.detailedError(
          "pagify.mission/get.description",
          "Error trying to get decription.",
          response
        );
        next("");
      },
    });
  },

  //
  //                              TITLE

  title: function (tree) {

    var acceptedTitles = [
      "title.jpg",
      "title.png",
      "title.pdf",
      "title-contain.jpg",
      "title-contain.png",
      "title.mov",
      "title.mp4",
    ];

    // for (var i = 0; i < acceptedTitles.length; i++) {
    //   if (_.findWhere(tree, { nameAndExtension: acceptedTitles[i] })) return acceptedTitles[i];
    // };

    return _.filter(tree, function(file){
      return _.contains(acceptedTitles, file.nameAndExtension);
    });

  },

  //
  //                              SLIDES

  slides: function (tree) {
    var slideFolder = _.findWhere(tree, { nameAndExtension: "slide", });
    if (slideFolder) return slideFolder.children;
  },

  //
  //                              GALLERY

  gallery: function (tree) {
    var galleryFolder = _.findWhere(tree, { nameAndExtension: "gallery", });
    if (galleryFolder) return galleryFolder.children;
  },

  //                              ¬
  //

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  GET ALL DATA
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION:
  ARGUMENTS: (
    options <{
      path <string>,
      tree <lego.ls result>,
    }>,
    callback <function({
      tags: <{}>,
      description: <string>,
      title: <string[]>,
      slides: <lego.ls result>,
      gallery: <lego.ls result>,
    })>,
  )
  RETURN:
*/
function getData (options, callback) {
  get.tags(options, function (tags) {
    get.description(options.path, tags, options.descriptionKey, function (description) {

      callback({
        tags: tags || {},
        description: description,
        titles: get.title(options.tree),
        slides: get.slides(options.tree),
        gallery: get.gallery(options.tree),
      });

    });
  });
}

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  PAGES MAKER
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION:
  ARGUMENTS: (
    pagy,
    data,
    options <{
      path <string>,
      ?tagsPatterns <tagPattern[]>,
      ?titleFunction: <string:default=title|function(tags):string> « key where to find title in tags, or function that returns the title »,
    }>,
  )
  RETURN:
*/
function pagesMaker (pagy, data, options) {

  if (!options) options = {};

  // DESCRIPTION PAGE
  pagy.createPage.description({
    description: data.description,
    tags: data.tags,
    tagsPatterns: options.tagsPatterns,
    thumbnailFolder: options.path,
  });

  // TITLE PAGES
  _.each(data.titles, function (d,i) {
    pagy.createPage.title({ url: d.path, tags: data.tags, titleFunction: options.titleFunction, });
  });
  // TITLER PAGE
  if (data.titlePager) pagy.createPage.title({ url: data.titlePager.path, tags: data.tags, });

  // SLIDES PAGES
  _.each(data.slides, function (d,i) {
    pagy.createPage.slide(d.path);
  });

  // GALLERY PAGES
  if (data.gallery) pagy.createPage.gallery({ entries: data.gallery, entriesThumbnails: data.gallery, }, { indicatorOptions: { type: "图", } });

}

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: display a set of pages description, title, slides, gallery
  ARGUMENTS: ({
    !$container: <yquerjObject>,
    !path: <string>,
    !tree: <output of lego.ls with recursive option to true>,
    !tagsPatterns: <tagPattern[]>,
    ?descriptionKey: <string> « if passed, will not fetch description from file but get it from tags' given key »,
    ?titleFunction: <string:default=title|function(tags):string> « key where to find title in tags, or function that returns the title »,
  })
  RETURN: void
  TYPES: tagPattern = <
    { name: <string>, noLabel: <boolean:default=false>} |
    function():return=string
  >
*/
pagify.mission = function (options) {
  getData(options, function (data) {

    // MAKE PAGIFY
    pagify.custom({
      $container: options.$container,
      maker: function () {
        pagesMaker(this, data, options);
      },
      options: { color: data.tags.color },
    });

  });
};

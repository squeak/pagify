var _ = require("underscore");
var $$ = require("squeak");
var pagify = require("./_");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

pagify.lib = {

  //
  //                              GRIDIFY

  gridify: function (pagy, options) {
    var defaultOptions = {
      thumbnailSize: 250,
      thumbnailPadding: 0,
      thumbnailMargin: 0,
      titleSize: 0,
      thumbnailTitleSize: 0,
    };
    for (key in defaultOptions) if (!options[key]) options[key] = defaultOptions[key];

    var thumbnailFullSize = options.thumbnailSize + 2*options.thumbnailMargin + 2*options.thumbnailPadding
    var containerWidth = pagy.container.width()
    var containerHeight = pagy.container.height() - options.titleSize

    var grid = {
      width: Math.floor(1*(containerWidth/thumbnailFullSize)),
      height: Math.floor(1*(containerHeight/(thumbnailFullSize+options.thumbnailTitleSize))),
    };

    grid.marginTop = ( containerHeight - (thumbnailFullSize * grid.height) ) / 2;
    return grid;
  },

  //
  //                              PAGIFY GALLERY

  pagifyGallery: function (pagy, entries, numberOfDisplayedEntries) {
    // copy the array so that the original is not modified
    var allEntries = entries.slice();

    //console.log(thumbnailsSize);
    //var numberOfDisplayedEntries = getNumberOfEntries(pagy.container, thumbnailsSize);
    //var numberOfDisplayedEntries = getNumberOfEntries (pagy.container, thumbnailsSize, thumbnailsMargin, thumbnailsPadding)
    var entriesByPage = [];

    // add piece of array every x entries
    while (allEntries.length > numberOfDisplayedEntries)
    entriesByPage.push(allEntries.splice(0, numberOfDisplayedEntries));
    entriesByPage.push(allEntries);

    return entriesByPage;
  },

  //                              ¬
  //

};

module.exports = pagify.lib;

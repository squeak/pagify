var _ = require("underscore");
var $$ = require("squeak");
var pagify = require("./_");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

pagify.indicator = {

  //
  //                              CREATE

  create: function (pagy, type, panid, className, options) { ////////////////////////////////// options pass in gallery for reload and in page indicator

    if (type == "point") type = "•" //●
    else if (type == "square") type = "■"
    else if (type == "number") type = " "+ (panid +1) +" "
    else if (type == "random") type = _.sample(["⊗", "⋈", "∑", "∅", "⋉", "⋊", "※", "↯", "⊕", "⋮", "⋕", "∝", "⟨", "|", "⟩", "⊧", "↦", "→", "⊆", "⊂", "⊇", "⊃", "⇒", "◅", "▻", "≻", "≺", "≪", "≫", "⇔", "↔", "≡", "≅", "≜", "≝", "≐", "≠", "∞", "∵", "∴", "∓", "±", "∮", "∫"]);

    options = $$.defaults(pagy.indicators.options, options);

    var missionColor = pagy.color; //|| "black";

    // append pageIndicator if first time
    if (pagy.container.find(".pagify-page_indicator").length == 0)
    pagy.indicators.$container = pagy.container.div({
      class: "pagify-page_indicator",
    })

    pagy.indicators.$container.a({
      id: panid,
      class: "indicator_button "+ className,
      html: type,
    }).css({
      color: function(){
        // if (panid == ssi(ssiName[pagy.id])) return "cyan"
        if (panid == pagy.panIdAsked) return options.color
        else return missionColor;
      },
      fontSize: options.size,
    }).click(function(){
      pagify.navigation.goToPage(pagy, $(this)[0].id);
    }).hover(function(){
      $(this).css("color", options.color);
    }, function(){
      // if ($(this)[0].id == ssi(ssiName[pagy.id])) $(this).css("color", "cyan");
      if ($(this)[0].id == pagy.panIdAsked) $(this).css("color", options.color);
      else $(this).css("color", pagy.color);
    });
  },

  //
  //                              PAGE INDICATOR CHANGE

  change: function (pagy, idAsked) {
    var missionColor = pagy.color || "black";

    pagy.indicators.$container.children().css({
      color: function(){
        if ($(this)[0].id == idAsked) return pagy.indicators.options.color
        else return missionColor;
      },
    });
  },

  //                              ¬
  //

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = pagify.indicator;

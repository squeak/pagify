window.jQuery = $;
require("jquery-colorbox");
var _ = require("underscore");
var $$ = require("squeak");
require("squeak/extension/url");
var pagify = require("./_");

function capitalizeFirstLetter (string) { return string.charAt(0).toUpperCase() + string.slice(1); };
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

pagify.models = {

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  EMPTY PAGE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: create an empty page in an existing pagify instance
    ARGUMENTS: (
      !pagy <pagy> « see pagify/index.js for pagy object definition »,
      !panid: <number>,
      ?options <{
        ?class: <string>,
        ?backgrounUrl: <string>,
        ?backgroundSize: <string>,
        ?negId: <boolean> « pass true to use panidNeg and not panid, in other words, to put the page before others »,
      }>,
    )
    RETURN: jqueryObject
  */
  empty: function (options) {

    var pagy = this;

    //
    //                              MAKE SURE OPTIONS OBJECT EXISTS

    if (!options) options = {};

    //
    //                              DETERMINE PAN ID TO USE

    var PANID = options.negId ? pagy.panidNeg : pagy.panid;

    //
    //                              APPEND PAGE

    var $targetPage = pagy.pan.div({
      class: "pagify-page",
      panid: PANID,
      width: pagy.container.width(),
      height: pagy.container.height(),
    }).css("left", function () { return pagy.container.width() * +PANID });

    // CUSTOM CLASS (IF ASKED)
    if (options.class) $targetPage.addClass(options.class);

    // CUSTOM BACKGROUND (IF ASKED)
    if (options.backgroundUrl) {

      // make sure necessary character are escaped to form a proper url
      // replace are to avoid problem with filenames containing quotes
      var cleanUrl = $$.url.urlify(options.backgroundUrl).replace(/\'/g, "\\'").replace(/\"/g, '\\"');

      $targetPage.css({
        background: "url('"+ cleanUrl +"') no-repeat center center",
        backgroundSize: options.backgroundSize,
      });

    };

    //
    //                              RETURN DIV

    return $targetPage;

    //                              ¬
    //

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  TAG PAGE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  tag: function (data, indicatorOptions) {

    var pagy = this;

    var tags = data;

    console.log(tags);

    return pagy;

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  DESCRIPTION PAGE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: create a page with a text description and specifications
    ARGUMENTS: (
      !data <{
        ?description: <htmlString>,
        ?tags: <object>,
        ?tagsPatterns: {
          [key]: <boolean|function> « boolean defines if key is displayed before or not, if function, the result of the function will be displayed »,
        },
        ?thumbnailFolder: <string>,
      }>,
      ?indicatorOption « see pagify.index for details »,
    )
    RETURN: pagy
  */
  description: function (data, indicatorOptions) {

    var pagy = this;

    var description = data.description || "";
    var tags = data.tags || {};

    // one more pan in left side
    pagy.panidNeg--;

    // append div
    var $thisPage = pagy.createPage.empty({ class: "description_page", negId: true, });

    //
    //                              DESCRIPTION CONTAINER

    var $descriptionContainer = $thisPage.div({
      class: "description-container",
    });

    //
    //                              DESCRIPTION CONTENT

    // make sure description exist
    if (description && description.status != 404) {
      description = description.replace(/\n/g,"<br/>"); //handle line breaks
      var $description = $descriptionContainer.div({
        class: "description-content",
        html: description,
      });
    };

    //
    //                              THUMBNAIL

    if (data.thumbnailFolder) {
      var descriptionPhoto = "url("+ data.thumbnailFolder +"thumbnail.png" +")" //router.extension(data.thumbnailFolder +"thumbnail")
      var $description = $thisPage.div({
        class: "thumbnail",
      }).css({
        backgroundImage: descriptionPhoto,
      });
    };

    //
    //                              TITLE AND TAGS

    var $titleAndTags = $thisPage.div({ class: "title_and_tags", });

    // append title
    $titleAndTags.span({
      html: tags.title,
    }).css({
      color: tags.color,
    });

    // append tags
    _.each(data.tagsPatterns, function (tagPattern) {

      if (_.isFunction(tagPattern)) var tagText = tagPattern(tags)
      else if (!tagPattern.noLabel && tags[tagPattern.key]) var tagText = "<b>"+ capitalizeFirstLetter(tagPattern.key) +":</b> "+ tags[tagPattern.key]
      else var tagText = tags[tagPattern.key];

      if (tagText) {
        $titleAndTags.br();
        $titleAndTags.span({
          class: "tag_text",
          html: tagText,
        });
      };

    });

    // append page indicator
    pagify.indicator.create(pagy, "文", pagy.panidNeg, "description_page", indicatorOptions); // Ω

    return pagy;

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  FICHE PAGE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  fiche: function (data, indicatorOptions) {

    var pagy = this;

    // page before first
    pagy.panidNeg--;
    // append div
    var $thisPage = pagy.createPage.empty({ class: "fiche_page", });
    $thisPage.css({
      backgroundColor: "grey",
    }).div({
      id: "fiche",
    }).css({
      // background: descriptionPhoto,
      // backgroundSize: "contain",
      color: "white",
      width: "60%",
      left: "30%",
      bottom: "20%",
      height: "70%",
      display: "block",
      position: "absolute",
      textAlign: "justify",
    }).div({
      html: data.text,
    }).css({
      fontFamily: "Lato",
      maxHeight: "100%",
      overflowY: "auto",
      display: "block",
      bottom: 0,
      position: "absolute",
    });
    // title and tags
    var titleAndTags = $thisPage.div({
      "class": "title",
    }).css({
      color: "#CCC",
      fontFamily: "Lato-Bold",
      textAlign: "left",
      marginLeft: "5%",
      width: "20%",
      position: "absolute",
      display: "block",
      bottom: "5%",
      padding: 0,
    });

    // append title
    titleAndTags.span({
      html: data.fiche.title,
    });
    // append fiche
    $.each(data.fiche, function(i,d){
      if (d) {
        titleAndTags.append("<br/>");
        titleAndTags.span({
          html: i +": ",
        });
        titleAndTags.span({
          html: d,
        }).css({
          fontFamily: "Lato-Light",
        });
      }
    });
    // append page indicator
    pagify.indicator.create(pagy, "文", pagy.panidNeg, "fiche_page", indicatorOptions); //Ω
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  TITLE PAGE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  title: function (data, indicatorOptions) {

    var pagy = this;

    var url = data.url;
    var tags = data.tags;

    // append div with background
    if (url.match(/(\.jpg$)|(\.png$)/)) {
      var $thisPage = pagy.createPage.empty({
        class: "title_page",
        backgroundUrl: url,
        backgroundSize: function () {
          if (url.match("-contain")) return "contain"
          else return "cover";
        },
      });
    }
    // pdf
    else if (url.match(/(\.pdf$)/)) {

      var $thisPage = pagy.createPage.empty({
        class: "title_page",
      });

      // make sure there is no scroll bar on the side
      $thisPage.css("overflow", "hidden");

      var $video = $thisPage.iframe({
        src: $$.url.urlify(url) +"#view=fitH",
      });
      // var $video = $thisPage.object({
      //   type: "application/pdf",
      //   data: $$.url.urlify(url),
      // });

    }
    // append div with video
    else {

      var $thisPage = pagy.createPage.empty({
        class: "title_page",
      });

      // make sure there is no scroll bar on the side
      $thisPage.css("overflow", "hidden");

      var $video = $thisPage.video().attr({
        controls: "",
        loop: "",
        autoplay: true,
      }).css({
        width: "100%",
        height: "100%",
      });

      $video.source({ src: url, });

    };

    // append title
    $thisPage.div({
      "class": "title",
      html: _.isFunction(data.titleFunction) ? data.titleFunction(tags) : tags[data.titleFunction || "title"],
    }).css("color", tags.color);

    // append page indicator
    pagify.indicator.create(pagy, "头", pagy.panid, "title_page", indicatorOptions); //
    // increase panid for next append
    pagy.panid++;
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  SLIDE PAGE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  slide: function (url, indicatorOptions) {

    var pagy = this;

    // append div with background
    var $thisPage = pagy.createPage.empty({
      class: "slide_page",
      backgroundUrl: url,
      backgroundSize: "contain",
    });

    // append page indicator
    pagify.indicator.create(pagy, "point", pagy.panid, "slide_page", indicatorOptions); //雪
    // increase panid for next append
    pagy.panid++;

    return pagy;

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  GALLERY PAGE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: make gallery pages
    ARGUMENTS: (
      !galleryEntries <{
        !entries: <{ path: string, }[]>
        !entriesThumbnails: < path: string, []>
      }>,
      options
    )
    RETURN: <pagy>
  */
  gallery: function (galleryEntries, options, resize) {

    var pagy = this;

    //
    //                              DEFAULT OPTIONS

    // set default values for options and apply them if they are not set
    var defaultOptions = {
      background: "random",
      backgroundSize: "cover",
      //grid: 8,
      thumbnailSize: 220,
      thumbnailPadding: 10,
      thumbnailMargin: 20,
      indicatorOptions: {
        _recursiveOption: true,
        pageIndicator: "point",
        pageIndicatorSize: 20,
      },
    };
    options = $$.defaults(defaultOptions, options);

    //                              ¬
    //

    // store gallery for usage in resize
    if (!resize) {
      pagy.gallery.push({ entries: galleryEntries.entries, entriesThumbnails: galleryEntries.entriesThumbnails });
      pagy.galleryOptions.push(options);
    };

    // get entries by page and set starting entry if it's out of entriesByPage range
    //var thumbnailsSize = determineThumbnailsSize(pagy, options.thumbnailSize, options.thumbnailMargin, options.thumbnailPadding);
    //if (options.numberOfPages) pagify.lib.gridify(pagy, "numberOfPages", options.numberOfPages, galleryEntries.entries) // don't use, it's not made
    //else var grid = pagify.lib.gridify(pagy, options.thumbnailSize, options.thumbnailMargin, options.thumbnailPadding, 0);
    var grid = pagify.lib.gridify(pagy, {thumbnailSize: options.thumbnailSize, thumbnailMargin: options.thumbnailMargin, thumbnailPadding: options.thumbnailPadding, thumbnailTitleSize: 0, titleSize: 0});
    //------ handle case in which height or width == 0
    var thumbnailSize = options.thumbnailSize;
    while (grid.height == 0 || grid.width == 0) {
      thumbnailSize -= 25;
      var grid = pagify.lib.gridify(pagy, {thumbnailSize: thumbnailSize, thumbnailMargin: options.thumbnailMargin, thumbnailPadding: options.thumbnailPadding, thumbnailTitleSize: options.thumbnailTitleSize, titleSize: options.titleSize});
    };
    //------ handle case in which height or width == 0 END
    var numberOfEntriesByPage = grid.width * grid.height;
    var entriesByPage = pagify.lib.pagifyGallery(pagy, galleryEntries.entries, numberOfEntriesByPage);
    var thumbnailsByPage = pagify.lib.pagifyGallery(pagy, galleryEntries.entriesThumbnails, numberOfEntriesByPage);

    // append every page
    for (KEY in entriesByPage) {
      // append div

      // CREATE PAGE
      var $thisPage = pagy.createPage.empty({
        class: "gallery_page",
        backgroundUrl: options.background == "random" ? (_.sample(entriesByPage[KEY]) || {}).path : options.background,
        backgroundSize: options.backgroundSize,
      });

      // append thumbnails in every page
      for (i in entriesByPage[KEY]) {
        var key = entriesByPage[KEY][i].path;

        // containers
        var entry = $thisPage.css({
          paddingTop: grid.marginTop,
          maxHeight: pagy.container.height(),
        }).div({
          id: key,
        }).css({
          //margin: "1%",
          //marginTop: "5%",
          //padding: "1%",
          margin: options.thumbnailMargin,
          padding: options.thumbnailPadding,
          display: "inline-block",
          backgroundColor: "rgba(255,255,255,0.6)",
        }).hover(function(){
          $(this).css("background-color", "rgba(0,255,255,0.3)");
        }, function(){
          $(this).css("background-color", "rgba(255,255,255,0.6)");
        });

        // links
        var A = entry.a({
          href: $$.url.urlify(key),
          title: key.replace(/^.*\//,""),
        }).css({
          background: 'rgba(0,0,0,0) url("'+ $$.url.urlify(thumbnailsByPage[KEY][i].path) +'") no-repeat 50% 50%',
          backgroundSize: "cover",
          display: "inline-block",
          width: thumbnailSize,
          height: thumbnailSize,
          //width: thumbnailsSize * 5/6,
          //height: thumbnailsSize * 5/6,
          //margin: "10%",
          //width: window.innerWidth/6,
          //height: window.innerWidth/6,
        });

        // make photo gallery (to see them in big)
        A.colorbox({rel:'imagesGroup', transition:"none", opacity: 0.5, maxWidth:"95%", maxHeight:"95%"});//.css('cursor','pointer');

        // titles
        //
        // entry.p({
        //   html: key.replace(/^.*\//,""),
        // }).css({
        //   color: "black",
        //   margin: 0,
        //   maxWidth: A.width(),
        //   //minHeight: thumbnailsSize * 1/6,
        //   fontSize: thumbnailsSize / 20,
        // });
      };

      // append page indicator with required icon
      pagify.indicator.create(pagy, options.indicatorOptions.type, pagy.panid, "gallery_page", options.indicatorOptions);
      // increase panid for next append
      pagy.panid++;

    };

    return pagy;

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = pagify.models;

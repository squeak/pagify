
var pagify = require("./_");

require("./floating");
require("./indicator");
require("./lib");
require("./models");
require("./mission");
require("./navigation");

module.exports = pagify;
